#include "WADFile.h"
#include <Valve.h>
#include <WouterUtility.h>
#include <string.h>
#include <iostream>

WADFile::WADFile(const char* filename)
{
    this->filename = filename;
}

WADFile::~WADFile()
{
	for (int i = 0; i < (int)this->lumps.size(); i++)
	{
		delete this->lumps[i];
	}
}

void WADFile::AddLump(Wouter::Utility::DataBlock* block)
{
	this->lumps.push_back(block);
}

void WADFile::WriteFile()
{
	if (this->lumps.size() > 0)
	{
        std::cout << "Writing WAD file: " << this->filename << "...";
		
		// Creating lumptable
		Valve::GoldSource::tWADLump* wadlumps = new Valve::GoldSource::tWADLump[this->lumps.size()];
		
        Wouter::Utility::DataFile wadfile(this->filename);
		
        if (wadfile.OpenForWriting())
		{
			// Creating Header
			Valve::GoldSource::tWADHeader header;
			strncpy(header.id, "WAD3", 4);
			header.numlumps = (int)this->lumps.size();
			header.tableoffset = 0;
			
			// Writing data
			wadfile.WriteData((void*)&header, sizeof(Valve::GoldSource::tWADHeader));
			for (int i = 0; i < (int)this->lumps.size(); i++)
			{
				if (this->lumps[i]->Data() != 0)
				{
					// Building lumptable
					wadlumps[i].compression = 0;
					wadlumps[i].disksize = this->lumps[i]->DataSize();
					if (this->lumps[i]->Data() != 0)
						strncpy(wadlumps[i].name, (char*)this->lumps[i]->Data(), 16);
					wadlumps[i].offset = wadfile.GetCurrentPosition();
					wadlumps[i].pad1 = 0;
					wadlumps[i].pad2 = 0;
					wadlumps[i].size = this->lumps[i]->DataSize();
					wadlumps[i].type = 0x43;
					wadfile.WriteData(this->lumps[i]);
				}
			}
			// Settings Lumptable offset
			header.tableoffset = wadfile.GetCurrentPosition();
			
			// Write lumptable and header
			wadfile.WriteData((void*)wadlumps, sizeof(Valve::GoldSource::tWADLump) * this->lumps.size());
			wadfile.WriteData((void*)&header, sizeof(Valve::GoldSource::tWADHeader), 0);
			
			wadfile.Close();
		}
		
        std::cout << "done!" << std::endl;
	}
	else
	{
        std::cout << "No textures found to write to " << this->filename << std::endl;
	}
}

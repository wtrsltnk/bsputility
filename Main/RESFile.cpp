#include "RESFile.h"
#include <WouterUtility.h>
#include <iostream>
#include <algorithm>

RESFile::RESFile(const std::string& filename, const std::vector<std::string>& ignorefiles, const std::string& modpath)
{
    this->filename = filename;
    this->ignorefiles = ignorefiles;
    this->modpath = modpath;
}

RESFile::~RESFile()
{ }

void RESFile::AddResource(const std::string& resource)
{
    std::string tmp = resource;
    std::transform(tmp.begin(), tmp.end(), tmp.begin(), ::tolower);
    if (std::find(this->ignorefiles.begin(), this->ignorefiles.end(), tmp) == this->ignorefiles.end())
    {
        if (Wouter::Utility::FileInfo(modpath + "\\" + resource).Exists())
            this->resources.insert(resource);
        else
            std::cout << "ignoring " << resource << ", does not exist in modpath" << std::endl;
    }
    else
        std::cout << "ignoring " << resource << ", its a default file" << std::endl;
}

void RESFile::WriteFile()
{
	if (this->resources.size() > 0)
	{
        std::cout << "Writing Resource file: " << this->filename << "...";
		
        Wouter::Utility::DataFile res(this->filename);
	
        if (res.OpenForWriting())
        {
            for (auto tmp : this->resources)
            {
                res.WriteData(tmp.c_str(), tmp.length());
				res.WriteData((void*)"\n", 1);
			}
			res.Close();
		}
		
        std::cout << "done!" << std::endl;
	}
	else
        std::cout << "No resources found to write to " << this->filename << std::endl;
}

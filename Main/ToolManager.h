#ifndef TOOLMANAGER_H_
#define TOOLMANAGER_H_

#include <vector>
#include <string>

typedef std::vector <std::string> StringList;

class ToolManager
{
protected:
	StringList directories;
	StringList bspfiles;
    StringList ignoreResFiles;
	bool generate_ent;
	bool generate_res;
    bool generate_wad;
    bool generate_zip;

    void ListBSPFiles(const std::string& directory);
	
public:
	ToolManager();
	virtual ~ToolManager();

	void MakeEntFile();
    void MakeResFile();
    void MakeWadFile();
    void MakeZipFile();
	
    void AddDirectory(const std::string& dir);
    void AddFile(const std::string& file);
    void IgnoreResFiles(const std::string& file);

	void RunTools();
};

#endif /*TOOLMANAGER_H_*/

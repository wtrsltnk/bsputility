#ifndef BSPTOOL_H_
#define BSPTOOL_H_

#include <WouterUtility.h>
#include <Valve.h>
#include <string>
#include "RESFile.h"
#include "WADFile.h"

class BSPTool
{
private:
    std::string filename;
	bool isopen;
	Wouter::Utility::DataFile* bspfile;
	Valve::GoldSource::tBSPHeader header;
	Wouter::Utility::DataBlock* entitydata;
    std::vector<std::string> ignorefiles;
	
	bool OpenBSP();
	void CloseBSP();

	Wouter::Utility::DataBlock* GetBSPLump(Valve::GoldSource::tBSPLump& lump);
	Wouter::Utility::DataBlock* GetMipLump(Wouter::Utility::DataBlock* texdata, int offset);
    void GetResource(Valve::GoldSource::tEntity* ent, const std::string& key, RESFile& res);
	void GetWorldSpawnResources(Valve::GoldSource::tEntity* ent, RESFile& res);
	
public:
    BSPTool(const std::string& filename, const std::vector<std::string>& ignorefiles);
	virtual ~BSPTool();
	
	bool WriteEntFile();
    bool WriteResFile();
	bool WriteWadFile();
	
};

#endif /*BSPTOOL_H_*/

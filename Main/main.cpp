#include <iostream>
#include <string>

#include "ToolManager.h"
#include "bsputility.h"

void PrintInfo();
void PrintUsage();

int main(int argc, char* argv[])
{
	ToolManager tools;
	
	PrintInfo();
	
	if (argc > 1)
	{
		for (int i = 0; i < argc; i++)
		{
            if (std::string(argv[i]) == "-f")
			{
				if (i < argc- 1)
				{
					tools.AddFile(argv[++i]);
				}
			}
            else if (std::string(argv[i]) == "-d")
			{
				if (i < argc- 1)
				{
					tools.AddDirectory(argv[++i]);
				}
			}
            else if (std::string(argv[i]) == "-ent")
			{
				tools.MakeEntFile();
			}
            else if (std::string(argv[i]) == "-res")
			{
				tools.MakeResFile();
            }
            else if (std::string(argv[i]) == "-wad")
            {
                tools.MakeWadFile();
            }
            else if (std::string(argv[i]) == "-ignore-files")
            {
                tools.IgnoreResFiles(argv[++i]);
            }
            else if (std::string(argv[i]) == "-help")
			{
				PrintUsage();
				return 0;
            }
            else if (std::string(argv[i]) == "-usage")
            {
                PrintUsage();
                return 0;
            }
		}
	}
	else
	{
        std::cout << "*** ERRROR ***: No parameters found" << std::endl << std::endl;
		PrintUsage();
	}
	
	tools.RunTools();
	
	return 0;
}

void PrintInfo()
{
    std::cout << BSPUTILITY_NAME << " version " << BSPUTILITY_VERSION << ", by Wouter Saaltink" << std::endl;
    std::cout << "Copyright (C) " << BSPUTILITY_DATE << " Wouter Saaltink" << std::endl << std::endl;
    std::cout << "This software is for the use with Valve's Software's Gold Source engine BSP and WAD files." << std::endl <<
                 "There is absolutely no warrenty for " << BSPUTILITY_NAME << "." << std::endl << std::endl;
}

void PrintUsage()
{
    std::cout << "" << BSPUTILITY_NAME << " usage:" << std::endl << std::endl;
    std::cout << "    > bsputility.exe [-f <filename>] [-d <directoryname>] [-ent] [-res] [-wad] [-help] [-usage]" << std::endl << std::endl;
    std::cout << "-f <filename>         specify the BSP file to utilize." << std::endl;
    std::cout << "-d <directoryname>    specify a directory with BSP files to utilize." << std::endl;
    std::cout << "-ent                  create an entity(.ENT) file from the BSP file(s)." << std::endl;
    std::cout << "-res                  create a resource(.RES) file from the BSP file(s)." << std::endl;
    std::cout << "-wad                  create a texture(.WAD) file from the BSP file(s)." << std::endl;
    std::cout << "-ignore-files         path to file containing files which get ignored in .res file." << std::endl;
    std::cout << "-help / -usage        Show this usage guide." << std::endl;
}

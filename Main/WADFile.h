#ifndef WADFILE_H_
#define WADFILE_H_

#include <WouterUtility.h>
#include <vector>
#include <string>

class WADFile
{
protected:
    std::string filename;
	std::vector <Wouter::Utility::DataBlock*> lumps;
	
public:
	WADFile(const char* filename);
	virtual ~WADFile();
	
	void AddLump(Wouter::Utility::DataBlock* block);
	void WriteFile();
};

#endif /*WADFILE_H_*/

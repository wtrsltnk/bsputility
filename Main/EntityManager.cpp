#include "EntityManager.h"
#include <string>
#include <iostream>

EntityManager::EntityManager(Wouter::Utility::DataBlock* entitydata)
{
	this->entitydata = entitydata;
	this->firstentity = 0;
	this->currententity = 0;
	
	this->ListEntities();
}

EntityManager::~EntityManager()
{
	llitem* item = this->firstentity;
	
	while (item != 0)
	{
		// Vorige item tijdelijk opslaan
		llitem* tmp = item;
		// Huidige item klaarzetten
		item = item->next;
		
		// Vorige item weggooien
		delete tmp->ent;
		delete tmp;
	}
}

void EntityManager::ListEntities()
{
	if (this->entitydata != 0)
	{
		if (this->tok.Setup(this->entitydata))
		{
			int res = tok.NextToken(this->token);
			
            while (res == TOKEN_READY && std::string("{") == this->token)
			{
				Valve::GoldSource::tEntity* entity = this->ParseEntity();
				
				if (entity != 0)
				{
					llitem* item = new llitem;
					item->ent = entity;
					item->next = this->firstentity;
					this->firstentity = item;
				}
				res = tok.NextToken(this->token);
			}
			this->currententity = this->firstentity;
		}
		else
		{
            std::cout << "Failed to setup tokenizer." << std::endl;
		}
	}
}

Valve::GoldSource::tEntity* EntityManager::ParseEntity()
{
	Valve::GoldSource::tEntity* entity = new Valve::GoldSource::tEntity;
	entity->epairs = 0;
	
	int res = this->tok.NextToken(this->token);
	
    while (res == TOKEN_READY && std::string("}") != this->token)
	{
		Valve::GoldSource::tEntityPair* pair = new Valve::GoldSource::tEntityPair;

		// Sla de key van de token op
        pair->key = this->token;

		// Sla de value van de token op
		this->tok.NextToken(this->token);
        pair->value = this->token;
		
		// Voeg het entitypair toe aan de entity
		pair->next = entity->epairs;
		entity->epairs = pair;
		
		// Volgende token zoeken
		this->tok.NextToken(this->token);
	}
	return entity;
}

Valve::GoldSource::tEntity* EntityManager::CurrentEntity()
{
	//  Als er geen huidig item is, return null 
	if (this->currententity == 0) return 0;

	// Entity van huidige item teruggeven
	return this->currententity->ent;
}

Valve::GoldSource::tEntity* EntityManager::NextEntity()
{
	//  Als er geen huidig item is, return null 
	if (this->currententity == 0) return 0;

	// Volgende huidige item opzoeken
	this->currententity = this->currententity->next;
	
	// Als er geen volgende huidig item is, return null
	if (this->currententity == 0) return 0;

	// Entity van huidige item teruggeven
	return this->currententity->ent;
}

Valve::GoldSource::tEntity* EntityManager::FindEntityByClassName(const std::string& classname)
{
    llitem* item = this->firstentity;

    // Itereer door de entity items
    while (item != 0)
    {
        // Zoek de classname waarde
        auto value = EntityManager::FindEntityValue(item->ent, "classname");

        // Als de classname waarde gelijk aan de classname string dan is de entity gevonden
        if (value == classname)
        {
            return item->ent;
        }
        item = item->next;
    }

	return 0;
}

std::string EntityManager::FindEntityValue(Valve::GoldSource::tEntity* entity, const std::string& key)
{
	if (entity != 0)
	{
		Valve::GoldSource::tEntityPair* e = entity->epairs;
		
		// Zoek het epair met de key string als key zolang er een epair is
        while (e != 0 && e->key != key)
		{
			e = e->next;
		}
		
		// Als er een epair gevonden is, geef de waarde terug
		if(e != 0)
		{
			return e->value;
		}
	}
	return "";
}

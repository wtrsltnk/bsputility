#ifndef ENTITYMANAGER_H_
#define ENTITYMANAGER_H_

#include <WouterUtility.h>
#include <Valve.h>

class EntityManager
{
private:
	struct llitem
	{
		llitem* next;
		Valve::GoldSource::tEntity* ent;
		
	};
	
private:
	Wouter::Utility::DataBlock* entitydata;
	llitem* firstentity;
	llitem* currententity;
	
	Wouter::Utility::Tokenizer tok;
	char token[MAX_TOKEN_SIZE];
	
	void ListEntities();
	Valve::GoldSource::tEntity* ParseEntity();
	
public:
	EntityManager(Wouter::Utility::DataBlock* entitydata);
	virtual ~EntityManager();

	Valve::GoldSource::tEntity* CurrentEntity();
	Valve::GoldSource::tEntity* NextEntity();
	
    Valve::GoldSource::tEntity* FindEntityByClassName(const std::string& classname);
    static std::string FindEntityValue(Valve::GoldSource::tEntity* entity, const std::string& key);
};

#endif /*ENTITYMANAGER_H_*/

#include "BSPTool.h"
#include "EntityManager.h"

#include <string>
#include <iostream>
#include <algorithm>

BSPTool::BSPTool(const std::string& filename, const std::vector<std::string>& ignorefiles) : isopen(false), ignorefiles(ignorefiles)
{
    this->filename = filename;
	
	this->isopen = this->OpenBSP();
	
	if (this->isopen)
	{
        std::cout << "Opening " << this->filename << std::endl;
	}
}

BSPTool::~BSPTool()
{
    this->CloseBSP();
    std::cout << "--------------------------------" << std::endl << std::endl;
}

bool BSPTool::OpenBSP()
{
	using namespace Wouter::Utility;
    this->bspfile = new DataFile(this->filename);
	
	if (this->bspfile->OpenForReading())
	{
		this->bspfile->ReadData((void*)&header, sizeof(Valve::GoldSource::tBSPHeader));
		
		if(header.version == Valve::GoldSource::BSPVERSION)
		{
			Valve::GoldSource::tBSPLump* lump = &this->header.lumps[Valve::GoldSource::BSPLUMP_ENTITIES]; 
			lump->size--; // NULL character weghalen
			this->entitydata = this->bspfile->ReadData(lump->size, lump->offset);
			return true;
		}
		else
		{
            std::cout << "*** ERROR *** : BSP version is wrong: " << header.version << " instead of " << Valve::GoldSource::BSPVERSION << std::endl;
		}
	}
	else
	{
        std::cout << "*** ERROR *** : Can\'t open BSP file " << this->filename << std::endl;
	}
	return false;
}

bool BSPTool::WriteEntFile()
{
	if (this->isopen)
	{
		// Formuleer de de volledige ent bestandsnaam
        std::string entfile =
                Wouter::Utility::FileInfo::GetPath(this->filename)
                + "\\" +
                Wouter::Utility::FileInfo::GetBaseName(this->filename)
                + ".ent";
		
		// Sla de entity data op
        std::cout << "Writing Entity file: " << entfile << "...";
		
		Wouter::Utility::DataBlock::ToFile(this->entitydata, entfile);
		
        std::cout << "done!" << std::endl;
		
		return true;
	}
	return false;
}

bool BSPTool::WriteResFile()
{
	if (this->isopen)
	{
		// Formuleer de de volledige res bestandsnaam
        std::string resfile =
                Wouter::Utility::FileInfo::GetPath(this->filename)
                + "\\" +
                Wouter::Utility::FileInfo::GetBaseName(this->filename)
                + ".res";

        std::string modpath = Wouter::Utility::FileInfo::GetPath(this->filename);
        modpath = Wouter::Utility::FileInfo::GetPath(modpath);

        RESFile res(resfile, this->ignorefiles, modpath);
		EntityManager eman(this->entitydata);
		
		Valve::GoldSource::tEntity* entity = eman.CurrentEntity();
		
		while (entity != 0)
		{
			// Vraag de classname van de huidige entity op
            auto classname = EntityManager::FindEntityValue(entity, "classname");
			
			// Op basis van de classname van de entity wordt de vervolg actie bepaald
            if (classname == "worldspawn")
			{
				this->GetWorldSpawnResources(entity, res);
			}
            else if (classname == "cycler_sprite" ||
                    classname == "env_sprite" ||
                    classname == "env_glow" ||
                    classname == "env_shooter")
			{
				this->GetResource(entity, "model", res);
			}
            else if (classname == "ambient_generic")
			{
				this->GetResource(entity, "messagge", res);
			}
			
			// Vraag de volgende entity op
			entity = eman.NextEntity();
		}

		res.WriteFile();
	}
	return true;
}

bool BSPTool::WriteWadFile()
{
	if (this->isopen)
	{
		Wouter::Utility::DataBlock* texs = 0;
		Valve::GoldSource::tBSPLump lump = this->header.lumps[Valve::GoldSource::BSPLUMP_TEXTURES];
		texs = this->GetBSPLump(lump);

		// Lees het aantal textures uit
		int numtextures;
		texs->ReadData((void*)&numtextures, sizeof(int));
		
		// Lees de testure offset table uit
		int *textureoffsets = new int[numtextures];
		texs->ReadData((void*)textureoffsets, sizeof(int) * numtextures);

		// Formuleer de de volledige wad bestandsnaam
		char wadfile[256] = { 0 };
        sprintf(wadfile, "%s\\%s.wad", Wouter::Utility::FileInfo::GetPath(this->filename.c_str()), Wouter::Utility::FileInfo::GetBaseName(this->filename.c_str()));
		
		// Wadbestand openen
		WADFile wad(wadfile);
		
		for (int i = 0; i < numtextures; i++)
		{
			// Lump toeveogen als deze bestaat
			Wouter::Utility::DataBlock* lump = this->GetMipLump(texs, textureoffsets[i]);
			if (lump != 0)
				wad.AddLump(lump);
		}
		
		// Wad bestand wegschrijven en sluiten
		wad.WriteFile();
		return true;
	}
	return false;
}

void BSPTool::CloseBSP()
{
	this->isopen = false;
	//delete this->entitydata;
}





Wouter::Utility::DataBlock* BSPTool::GetBSPLump(Valve::GoldSource::tBSPLump& lump)
{
	Wouter::Utility::DataBlock* result = 0;
	
	if (this->isopen)
	{
		result = this->bspfile->ReadData(lump.size, lump.offset);
	}
	
	return result;
}

Wouter::Utility::DataBlock* BSPTool::GetMipLump(Wouter::Utility::DataBlock* texdata, int offset)
{
	Wouter::Utility::DataBlock* result = 0;

	Valve::GoldSource::tMiptex mip;
	texdata->ReadData((void*)&mip, sizeof(Valve::GoldSource::tMiptex), offset);
	
	if (mip.offset[0] > 0)
	{
		int size = mip.width * mip.height;
        int datasize = size + (size/4) + (size/16) + (size/64);
        
        // Lees de echte palette grootte uit(waarschijnlijk 256 kleuren)
        short palettesize = 256;
        texdata->ReadData((void*)&palettesize, sizeof(short), offset + sizeof(Valve::GoldSource::tMiptex) + datasize);

        // Bereken de lumpgrootte. Vergeet daarbij neit dat de palettesize * 3 moet, om dat er 3 bytes per kleur gebruikt worden
        int lumpsize = sizeof(Valve::GoldSource::tMiptex) + datasize + sizeof(int) + (palettesize * 3);
        
        // Lees de complete lump uit
        result = texdata->ReadData(lumpsize, offset);
	}
	
	return result;
}

void BSPTool::GetResource(Valve::GoldSource::tEntity* ent, const std::string& key, RESFile& res)
{
    auto model = EntityManager::FindEntityValue(ent, key);
	
    if (model.length() > 0)
    {
        std::replace(model.begin(), model.end(), '/', '\\');
        res.AddResource(model.c_str());
	}
}

void BSPTool::GetWorldSpawnResources(Valve::GoldSource::tEntity* ent, RESFile& res)
{
    auto wads = EntityManager::FindEntityValue(ent, "wad");
    auto skyname = EntityManager::FindEntityValue(ent, "skyname");
	
    if (wads.length() > 0)
	{
		char wad[256] = { 0 };
		int wi = 0;

        for (int i = 0; i < int(wads.length()); i++)
		{
			if (wads[i] == ';')
			{
                wad[wi] = '\0';
                std::string tmp = wad;
                std::replace(tmp.begin(), tmp.end(), '/', '\\');
                res.AddResource(Wouter::Utility::FileInfo::GetName(tmp));
				wi = 0;
			}
			else
			{
				wad[wi++] = wads[i];
			}
		}
	}
	else
	{
        std::cout << "No wad specification found" << std::endl;
	}
	
    if (skyname.length() > 0)
	{
        res.AddResource(std::string("gfx\\env\\") + skyname + "bk.tga");
        res.AddResource(std::string("gfx\\env\\") + skyname + "dn.tga");
        res.AddResource(std::string("gfx\\env\\") + skyname + "ft.tga");
        res.AddResource(std::string("gfx\\env\\") + skyname + "lf.tga");
        res.AddResource(std::string("gfx\\env\\") + skyname + "rt.tga");
        res.AddResource(std::string("gfx\\env\\") + skyname + "up.tga");
	}
	else
	{
        std::cout << "No sky specification found" << std::endl;
	}
}


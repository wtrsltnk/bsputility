#ifndef RESFILE_H_
#define RESFILE_H_

#include <vector>
#include <set>
#include <string>
#include <windows.h>
#include "bsputility.h"

#define RESOURCE_HEADER "// This Resource file was created using\n// %s version %s, by Wouter Saaltink\n// Copyright (C) %s Wouter Saaltink\n\n"

class RESFile
{
protected:
    std::string filename;
    std::set<std::string> resources;
    std::vector<std::string> ignorefiles;
    std::string modpath;
	
public:
    RESFile(const std::string& filename, const std::vector<std::string>& ignorefiles, const std::string& modpath);
	virtual ~RESFile();

    void AddResource(const std::string& resource);
	void WriteFile();
};

#endif /*RESFILE_H_*/

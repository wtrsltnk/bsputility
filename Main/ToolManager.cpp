#include "ToolManager.h"
#include "BSPTool.h"
#include <iostream>
#include <algorithm>
#include <windows.h>
#include <WouterUtility.h>

ToolManager::ToolManager()
{
	this->generate_ent = false;
	this->generate_res = false;
	this->generate_wad = false;
}

ToolManager::~ToolManager()
{ }

void ToolManager::ListBSPFiles(const std::string& directory)
{
	char dir[256] = { 0 };
	WIN32_FIND_DATA find;
	BOOL done = TRUE;
	
    sprintf(dir, "%s\\*.bsp", directory.c_str());
	HANDLE handle = ::FindFirstFile(dir, &find);
	
	done = handle != INVALID_HANDLE_VALUE ? TRUE : FALSE;
	
	while (done == TRUE)
	{
        this->AddFile(directory + "\\" + find.cFileName);
		done = ::FindNextFile(handle, &find);
	}
	::FindClose(handle);
}

void ToolManager::MakeEntFile()
{
	this->generate_ent = true;
}

void ToolManager::MakeResFile()
{
	this->generate_res = true;
}

void ToolManager::MakeWadFile()
{
	this->generate_wad = true;
}

void ToolManager::AddDirectory(const std::string& dir)
{
    this->directories.push_back(dir);
}

void ToolManager::AddFile(const std::string& file)
{
    if (Wouter::Utility::FileInfo::GetExtension(file) == ".bsp")
    {
        this->bspfiles.push_back(file);
	}
}

std::string& trim_right_inplace(std::string& s, const std::string& delimiters = " \f\n\r\t\v")
{
    return s.erase( s.find_last_not_of( delimiters ) + 1 );
}

std::string& trim_left_inplace(std::string& s, const std::string& delimiters = " \f\n\r\t\v")
{
    return s.erase( 0, s.find_first_not_of( delimiters ) );
}

std::string& trim_inplace(std::string& s, const std::string& delimiters = " \f\n\r\t\v")
{
    return trim_left_inplace( trim_right_inplace( s, delimiters ), delimiters );
}

void ToolManager::IgnoreResFiles(const std::string& file)
{
    std::cout << "Ignoring all files from " << file << " in resource files." << std::endl;
    std::vector<std::string> lines = Wouter::Utility::FileInfo::ReadAllLines(file);
    for (std::string& s : lines)
    {
        std::replace(s.begin(), s.end(), '/', '\\');
        std::transform(s.begin(), s.end(), s.begin(), ::tolower);
        trim_inplace(s);
    }
    ignoreResFiles.insert(ignoreResFiles.end(), lines.begin(), lines.end());
}

void ToolManager::RunTools()
{
	for (int i = 0; i < (int)this->directories.size(); i++)
	{
        this->ListBSPFiles(this->directories[i]);
	}
	
	for (int i = 0; i < (int)this->bspfiles.size(); i++)
	{
        BSPTool tool(this->bspfiles[i], this->ignoreResFiles);

		if (this->generate_ent) tool.WriteEntFile();
        if (this->generate_res) tool.WriteResFile();
		if (this->generate_wad) tool.WriteWadFile();
	}
}

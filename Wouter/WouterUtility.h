// 
// File:   Utility.h
// Author: wouter
//
// Created on 23 november 2007, 8:39
//

#ifndef _UTILITY_H
#define	_UTILITY_H

#include <string>
#include <vector>

#ifndef MAX_PATH
#define MAX_PATH 256
#endif

#define ILLEGAL_PATH_CHARS ':*?\"<>|'

#define MAX_TOKEN_SIZE 1024

#define TOKEN_EOF -1
#define TOKEN_READY 0
#define TOKEN_ERROR 1

#define IS_QUOTED_TOKEN(ptr) (ptr == '"')
#define IS_COMMENT_TOKEN(ptr) (ptr == '#' || ptr == ';')
#define IS_SPACE_TOKEN(ptr) (ptr <= 32)
#define IS_NEWLINE_TOKEN(ptr) (ptr == '\n')

namespace Wouter
{
    namespace Utility
    {
		/*
		* Wouter::Utility::DataBlock class
		*/
		class DataBlock
		{
		protected:
			void* pData;
			int nSize;
			int nPosition;
		
		public:
			DataBlock();
			DataBlock(DataBlock& data);
			DataBlock(int size);
			DataBlock(void* data, int size);
			virtual ~DataBlock();
			
			/*
			* Accessors voor pData, nSize en nPosition
			*/
			const void* Data() const;
			int DataSize() const;
			int Position() const;
			
			/*
			* Als offset -1 is, moet het data block vanaf nPosition gelezen worden
			*/
			virtual DataBlock* ReadData(int size, int offset = -1);
			
			/*
			* Als offset -1 is, moet het data block vanaf nPosition gelezen worden
			*/
			virtual void ReadData(void* data, int size, int offset = -1);
			
			/*
			* Als offset -1 is, moet het data block vanaf nPosition toegevoegd worden
			*/
			virtual bool WriteData(DataBlock* block, int offset = -1);
			
			   /*
			* Als offset -1 is, moet het data block vanaf nPosition toegevoegd worden
			*/
			bool WriteData(void* data, int size, int offset = -1);
		
		public:
			/*
			* Als offset -1 is moet er geen ::fseek gebruikt worden en moet de data
			* gewoon vanaf de huidge positie in de file gelezen worden
			*/
			static DataBlock* FromFile(const std::string& filename, int offset = -1);
			
			/*
			* Als offset -1 is moet er geen ::fseek gebruikt worden en moet de data
			* gewoon vanaf de huidge positie in de file gelezen worden
			*/
			static DataBlock* FromFile(FILE* file, int size, int offset = -1);

			/*
			* Als offset -1 is moet er geen ::fseek gebruikt worden en moet de data
			* gewoon op de huidge positie in de file geschreven worden
			*/
			static bool ToFile(DataBlock* block, const std::string& filename, int offset = -1);
			
			/*
			* Als offset -1 is moet er geen ::fseek gebruikt worden en moet de data
			* gewoon op de huidge positie in de file geschreven worden
			*/
			static bool ToFile(DataBlock* block, FILE* file, int offset = -1);
		};

        
        
        /*
         * Wouter::Utility::DataFile class
         */
        class DataFile
        {
        protected:
			std::string szFilename;
            FILE* hFile;
            int nSize;
            
			void SetFilename(const std::string& filename);

        public:
            DataFile();
            DataFile(DataFile& file);
			DataFile(const std::string& filename);
            virtual ~DataFile();

            /*
             * Als de filename ongelijk aan 0 is, wordt het bestand met filename
             * geopend, anders wordt de szFilename gebruikt
             */
			bool OpenForReading();
			bool OpenForWriting();

            /*
             * Vraag de huidige filepositie op (ftell())
             */
            int GetCurrentPosition();

            /*
             * Als offset -1 is, moet het data block vanaf de huidige positie gelezen worden
             */
            DataBlock* ReadData(int size, int offset = -1);

            /*
             * Als offset -1 is, moet het data block vanaf de huidige positie gelezen worden
             */
			void ReadData(void* data, int size, int offset = -1);

            /*
             * Als offset -1 is, moet er geen fseek uitgevoerd worden en kan de data
             * gewoon op de huidige file pointer positie weggeschreven worden
             */
			bool WriteData(const DataBlock* block, int offset = -1);

            /*
             * Als offset -1 is, moet er geen fseek uitgevoerd worden en kan de data
             * gewoon op de huidige file pointer positie weggeschreven worden
             */
			bool WriteData(const void* data, int size, int offset = -1);

            /*
             * Als Close() niet aangeroepen wortd voordat het DataFileobject verwijderd
             * wordt, wordt Close() in de deconstructor aangeroepen.
             */
            void Close();
        };
        
        
        
        /*
         * Wouter::Utility::FileInfo class
         */
        class FileInfo
        {
        private:
			std::string szFilename;
            
        public:
            FileInfo(FileInfo& file);
			FileInfo(const std::string& filename);
            virtual ~FileInfo();
            
            bool Exists();
            
			std::string FullName();
			std::string Name();
			std::string BaseName();
			std::string Path();
			std::string Extension();
            
            DataBlock* Read();
            bool Write(DataBlock* block);
			bool Append(DataBlock* block);
            
        public:
			static bool Exists(const std::string& src);
			static std::string GetName(const std::string& src);
			static std::string GetBaseName(const std::string& src);
			static std::string GetPath(const std::string& src);
			static std::string GetExtension(const std::string& src);
			static std::vector<std::string> ReadAllLines(const std::string& filename);
        };
        
        
        
        /*
         * Wouter::Utility::DirectoryInfo class
         */
        class DirectoryInfo
        {
        private:
			std::string szPath;
            
        public:
            DirectoryInfo(DirectoryInfo& dir);
			DirectoryInfo(const std::string& path);
            virtual ~DirectoryInfo();
            
            bool Exists();
            
			std::string FullName();
			std::string Name();
            
        public:
			static bool Exists(const std::string& path);
			static std::string GetName(const std::string& path);
        };
        
        
        
        /*
         * Wouter::Utility::Tokenizer class
         */
        class Tokenizer
        {
        protected:
        	char* data;
        	int datasize;
        	
        	char* ptr;
        	char* end;
        	
        	int line;
        	
        	bool ready;
        	
        	int SkipSpaces();
			int TokenError(const char* err);
        	
        public:
        	Tokenizer();
        	virtual ~Tokenizer();

        	bool Setup(DataBlock* block);
        	int NextToken(char* token, int maxtokensize = MAX_TOKEN_SIZE);
        	int IsReady();
        };
    }
}

#endif	/* _UTILITY_H */


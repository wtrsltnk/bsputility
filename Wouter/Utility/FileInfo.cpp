#include <WouterUtility.h>
#include <string.h>

using namespace Wouter::Utility;

FileInfo::FileInfo(const std::string& filename)
{
    this->szFilename = filename;
    
    for (int i = 0; i < this->szFilename.length(); i++)
    {
        if (this->szFilename[i] == '/')
            this->szFilename[i] = '\\';
    }
}

FileInfo::~FileInfo()
{ }

DataBlock* FileInfo::Read()
{
    return DataBlock::FromFile(this->szFilename);
}

bool FileInfo::Write(DataBlock* block)
{
    return DataBlock::ToFile(block, this->szFilename, 0);
}

bool FileInfo::Append(DataBlock* block)
{
    return DataBlock::ToFile(block, this->szFilename);
}

bool FileInfo::Exists(const std::string& filename)
{
    FILE* file =  ::fopen(filename.c_str(), "r");
    
    if (file != 0)
    {
        ::fclose(file);
        return true;
    }
    return false;
}

std::string FileInfo::GetName(const std::string& src)
{
    std::string result;
	bool haspath = false;
	
    for (int i = src.length(); i >= 0; i--)
	{
		if (src[i] == '\\' || src[i] == '/')
		{
			haspath = true;
            i++;
            for (int j = 0; (j + i) < src.length(); j++)
			{
                result += src[i + j];
            }
			break;
		}
	}
	if (!haspath)
	{
        result = src;
	}
	return result;
}

std::string FileInfo::GetBaseName(const std::string& src)
{
    std::string result;
    bool haspath = false;
	
    for (int i = src.length(); i >= 0; i--)
	{
		if (src[i] == '\\' || src[i] == '/')
		{
			haspath = true;
			i++;
            for (int j = 0; (j + i) < src.length(); j++)
			{
				if (src[i + j] == '.') break;
				
                result += src[i + j];
            }
			break;
		}
	}
	if (!haspath)
	{
        for (int j = 0; j < src.length(); j++)
		{
			if (src[j] == '.') break;
			
            result += src[j];
        }
	}
	return result;
}

std::string FileInfo::GetPath(const std::string& src)
{
    std::string result;

    for (int i = src.length(); i >= 0; i--)
	{
		if (src[i] == '\\' || src[i] == '/')
		{
            for (int j = 0; j < i; j++)
			{
                result += src[j];
			}
			break;
		}
    }
	return result;
}

std::string FileInfo::GetExtension(const std::string& src)
{
    std::string result;
	
    for (int i = src.length(); i >= 0; i--)
	{
		if (src[i] == '.')
		{
            for (int j = 0; (j + i) < src.length(); j++)
			{
                result += src[i + j];
			}
			break;
		}
    }
	return result;
}

std::vector<std::string> FileInfo::ReadAllLines(const std::string& filename)
{
    std::vector<std::string> result;

    FILE* file = fopen(filename.c_str(), "rb");

    if (file != 0)
    {
        char line[256];
        while (fgets(line, sizeof(line), file))
        {
            result.push_back(line);
        }
        fclose(file);
    }

    return result;
}

bool FileInfo::Exists()
{
    return FileInfo::Exists(this->szFilename);
}

std::string FileInfo::FullName()
{
    return this->szFilename;
}

std::string FileInfo::Name()
{
    return FileInfo::GetName(this->szFilename);
}

std::string FileInfo::Path()
{
    return FileInfo::GetPath(this->szFilename);
}

std::string FileInfo::Extension()
{
    return FileInfo::GetExtension(this->szFilename);
}

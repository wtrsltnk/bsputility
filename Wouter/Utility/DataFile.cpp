#include <WouterUtility.h>
#include <string.h>     // voor memcpy() en memset()
#include <stdlib.h>     // voor malloc() en free()

using namespace Wouter::Utility;

DataFile::DataFile() : hFile(0), nSize(0)
{ }

DataFile::DataFile(DataFile& file) : hFile(file.hFile), nSize(file.nSize)
{
	this->SetFilename(file.szFilename);
}

DataFile::DataFile(const std::string& filename) : hFile(0), nSize(0)
{
	this->SetFilename(filename);
}

DataFile::~DataFile()
{
	this->hFile = 0;
	this->nSize = 0;
}

void DataFile::SetFilename(const std::string& filename)
{
    this->szFilename = filename;
}

bool DataFile::OpenForReading()
{
    this->hFile = ::fopen(this->szFilename.c_str(), "rb");
	
	return (this->hFile != 0);
}

bool DataFile::OpenForWriting()
{
    this->hFile = ::fopen(this->szFilename.c_str(), "wb");
	
	return (this->hFile != 0);
}

int DataFile::GetCurrentPosition()
{
	if (this->hFile != 0)
	{
		return (int)::ftell(this->hFile);
	}
	return 0;
}

DataBlock* DataFile::ReadData(int size, int offset)
{
	DataBlock* result = 0;
	
	if (size > 0)
	{
		void* data = malloc(size);
		if (data != 0)
		{
			this->ReadData(data, size, offset);
			result = new DataBlock(data, size);
		}
	}
	return result;
}

void DataFile::ReadData(void* data, int size, int offset)
{
	if (this->hFile != 0)
	{
		if (offset >= 0)
		{
			::fseek(this->hFile, offset, SEEK_SET);
		}
		
		::fread(data, 1, size, this->hFile);
	}
}

bool DataFile::WriteData(const DataBlock* block, int offset)
{
	if (block != 0)
	{
		if (offset >= 0)
		{
			::fseek(this->hFile, offset, SEEK_SET);
		}
		
		return ((int)::fwrite(block->Data(), 1, block->DataSize(), this->hFile) == block->DataSize());
	}
	return false;
}

bool DataFile::WriteData(const void* data, int size, int offset)
{
	if (size > 0)
	{
		if (offset >= 0)
		{
			::fseek(this->hFile, offset, SEEK_SET);
		}
		
		return ((int)::fwrite(data, 1, size, this->hFile) == size);
	}
	return false;
}

void DataFile::Close()
{
	if (this->hFile != 0)
	{
		::fclose(this->hFile);
		this->hFile = 0;
		this->nSize = 0;
	}
}

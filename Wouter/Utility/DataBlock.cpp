#include "WouterUtility.h"
#include <string.h>     // voor memcpy() en memset()
#include <stdlib.h>     // voor malloc() en free()

using namespace Wouter::Utility;

DataBlock::DataBlock() : pData(0), nSize(0), nPosition(0)
{
}

DataBlock::DataBlock(DataBlock& data)
{
    this->nPosition = 0;
    if (data.nSize > 0)
    {
        this->pData = malloc(data.nSize);
        this->nSize = data.nSize;
        
        memcpy(this->pData, data.pData, data.nSize);
    }
    else
    {
        this->pData = 0;
        this->nSize = 0;
    }
}

DataBlock::DataBlock(int size)
{
    this->nPosition = 0;
    if (size > 0)
    {
        this->pData = malloc(size);
        this->nSize = size;
        
        memset(this->pData, 0, size);
    }
    else
    {
        this->pData = 0;
        this->nSize = 0;
    }
}

DataBlock::DataBlock(void* data, int size)
{
    this->nPosition = 0;
    if (size > 0)
    {
        this->pData = malloc(size);
        this->nSize = size;
        
        memcpy(this->pData, data, size);
    }
    else
    {
        this->pData = 0;
        this->nSize = 0;
    }
}

DataBlock::~DataBlock()
{
    if (this->pData != 0)
    {
        free(this->pData);
    }
    this->pData = 0;
    this->nSize = 0;
    this->nPosition = 0;
}

const void* DataBlock::Data() const
{
    return this->pData;
}

int DataBlock::DataSize() const
{
    return this->nSize;
}

int DataBlock::Position() const
{
    return this->nPosition;
}

DataBlock* DataBlock::ReadData(int size, int offset)
{
    DataBlock* result = 0;
    
    // Gebruik de juiste offset
    if (offset < 0)
        offset = this->nPosition;
    else
        this->nPosition = offset;
    
    // Als de gevraagde data bestaat, lees dit dan uit en maak er een DataBlock van
    if ((offset + size) <= this->nSize)
    {
        result = new DataBlock((char*)this->pData + offset, size);
        this->nPosition += size;        // Schuif de cursor op na de lees actie
    }
    
    return result;
}

void DataBlock::ReadData(void* data, int size, int offset)
{
    // Gebruik de juiste offset
    if (offset < 0)
        offset = this->nPosition;
    else
        this->nPosition = offset;

    // Als de gevraagde data bestaat, lees dit dan uit en maak er een DataBlock van
    if ((offset + size) <= this->nSize)
    {
        memcpy(data, (char*)this->pData + offset, size);
        this->nPosition += size;        // Schuif de cursor op na de lees actie
    }
}

bool DataBlock::WriteData(DataBlock* block, int offset)
{
    if (block != 0)
    {
        return this->WriteData(block->pData, this->nSize, offset);
    }
    return false;
}

bool DataBlock::WriteData(void* data, int size, int offset)
{
    if (data != 0 && size > 0)
    {
        // Gebruik de juiste offset
        if (offset < 0)
            offset = this->nPosition;
        else
            this->nPosition = offset;
        
        // Zorg ervoor dat het datablock groot genoeg is om de nieuwe data in
        // te zetten
        if ((offset + size) > this->nSize)
        {
            int newsize = offset + size;
            void* newdata = malloc(newsize);
            
            memcpy(newdata, this->pData, this->nSize);
            free(this->pData);
            
            this->pData = newdata;
            this->nSize = newsize;
        }
        
        // Kopieer de nieuwe data
        memcpy((void*)((char*)this->pData + offset), data, size);
        this->nPosition += size;        // Schuif de cursor op na de schrijf actie
        
        return true;
    }
    return false;
}

DataBlock* DataBlock::FromFile(const std::string& filename, int offset)
{
    FILE* file =fopen(filename.c_str(), "rb");
    DataBlock* result = 0;
    
    if (file != 0)
    {
       fseek(file, 0, SEEK_END);
        int size =ftell(file);
       fseek(file, 0, SEEK_SET);
        
        result = DataBlock::FromFile(file, size, offset);
        
       fclose(file);
    }
    return result;
}

DataBlock* DataBlock::FromFile(FILE* file, int size, int offset)
{
    DataBlock* result = 0;
    
    if (file != 0)
    {
        if (offset >= 0)
        {
           fseek(file, offset, SEEK_SET);
        }
        
        void* data = malloc(size);
        
        if (data != 0)
        {
           fread(data, 1, size, file);
        
            result = new DataBlock(data, size);
        }
    }
    return result;
}

bool DataBlock::ToFile(DataBlock* block, const std::string& filename, int offset)
{
    bool result = false;
    
    if (block != 0)
    {
        FILE* file =fopen(filename.c_str(), "wb");
        
        if (file != 0)
        {
            result = DataBlock::ToFile(block, file, offset);
            
           fclose(file);
        }
    }
    return result;
}

bool DataBlock::ToFile(DataBlock* block, FILE* file, int offset)
{
    if (block != 0 && file != 0)
    {
        if (offset >= 0)
        {
           fseek(file, offset, SEEK_SET);
        }
        
        if (::fwrite(block->pData, 1, block->nSize, file) == (unsigned int)block->nSize)
        {
            return true;
        }
    }
    return false;
}

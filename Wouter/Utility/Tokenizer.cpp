#include <WouterUtility.h>
#include <memory.h>
#include <stdlib.h>
#include <stdio.h>

using namespace Wouter::Utility;

Tokenizer::Tokenizer()
{
	this->data = 0;
	this->datasize = 0;
	this->ptr = 0;
	this->end = 0;
	this->line = 1;
	this->ready = false;
}

Tokenizer::~Tokenizer()
{
	this->data = 0;
	this->datasize = 0;
	this->ptr = 0;
	this->end = 0;
	this->line = 1;
	this->ready = false;
}

bool Tokenizer::Setup(DataBlock* block)
{
	if (block->DataSize() > 0)
	{
		this->data = (char*)block->Data();
		this->datasize = block->DataSize();
		
		this->ptr = this->data;
		this->end = this->data + this->datasize;
		this->line = 1;

		this->ready = true;
		
		return true;
	}
	return false;
}

int Tokenizer::NextToken(char* token, int maxtokensize)
{
	char* result = token;
	int res = 0;
	
	if (token != 0 && this->ready)
	{
		// Check for end of script
		if (this->ptr >= this->end)
			return TOKEN_EOF;
		
		// Skip all space characters
		if ((res = this->SkipSpaces()) != TOKEN_READY)
			return res;
		
		// What kind of token is it?
		if (IS_COMMENT_TOKEN(*this->ptr))
		{
			// Comment line
			while (*this->ptr != '\n')
			{
				this->ptr++;
				if (this->ptr >= this->end)
					return TOKEN_EOF;
				
				this->line++;
			}
			return NextToken(token, maxtokensize);
		}
		else if (IS_QUOTED_TOKEN(*this->ptr))
		{
			// Quoted token
			this->ptr++;
			while (!IS_QUOTED_TOKEN(*this->ptr))
			{
				if (this->ptr >= this->end)
					break;
				
				if (maxtokensize <= 0)
					return this->TokenError("max token size reached");
				
				*result++ = *this->ptr++;
				*result = 0;
				maxtokensize--;
			}
			this->ptr++;
			return TOKEN_READY;
		}
		else
		{
			// Regular token
			while (!IS_SPACE_TOKEN(*this->ptr) && !IS_COMMENT_TOKEN(*this->ptr))
			{
				if (this->ptr >= this->end)
					break;
				
				if (maxtokensize <= 0)
					return this->TokenError("max token size reached");
				
				*result++ = *this->ptr++;
				*result = 0;
				maxtokensize--;
			}
			return TOKEN_READY;
		}
	}
	else
	{
		return this->TokenError("token is NULL or tokenizer not ready");
	}
	return TOKEN_ERROR;
}

int Tokenizer::IsReady()
{
	if (this->ready)
	{
		return TOKEN_READY;
	}
	return TOKEN_ERROR;
}

int Tokenizer::SkipSpaces()
{
	while (IS_SPACE_TOKEN(*this->ptr))
	{
		if (this->ptr >= this->end)
			return TOKEN_EOF;
		
		if (IS_NEWLINE_TOKEN(*this->ptr))
			this->line++;

		this->ptr++;
	}
	return TOKEN_READY;
}

int Tokenizer::TokenError(const char* err)
{
	printf("*** ERROR *** on line %d : %s\n", this->line, err);
	
	return TOKEN_ERROR;
}

#include <WouterUtility.h>
#include <string.h>
#include <io.h>			// For access().
#include <sys/types.h>	// For stat().
#include <sys/stat.h>	// For stat().

using namespace Wouter::Utility;

DirectoryInfo::DirectoryInfo(DirectoryInfo& dir)
{
    this->szPath = dir.szPath;
    
    for (int i = 0; i < this->szPath.length(); i++)
    {
        if (this->szPath[i] == '/')
            this->szPath[i] = '\\';
    }
}

DirectoryInfo::DirectoryInfo(const std::string& path)
{
    this->szPath = path;
    
    for (int i = 0; i < this->szPath.length(); i++)
    {
        if (this->szPath[i] == '/')
            this->szPath[i] = '\\';
    }
}

DirectoryInfo::~DirectoryInfo()
{ }

bool DirectoryInfo::Exists(const std::string& path)
{
    if (::access(path.c_str(), 0) == 0)
	{
		struct stat status;
        ::stat(path.c_str(), &status);
	
	    if (status.st_mode & S_IFDIR )
	    {
	        return true;
	    }
	}
    return false;
}

std::string DirectoryInfo::GetName(const std::string& src)
{
    std::string result = src;
	bool haspath = false;
	
    for (int i = src.length(); i >= 0; i--)
	{
		if (src[i] == '\\' || src[i] == '/')
		{
			haspath = true;
			i++;
			int j = 0;
            for (j = 0; (j + i) < src.length(); j++)
			{
				result[j] = src[i + j];
			}
			result[j] = '\0';
			break;
		}
	}
	if (!haspath)
	{
        result = src;
	}
	return result;
}

bool DirectoryInfo::Exists()
{
    return DirectoryInfo::Exists(this->szPath);
}

std::string DirectoryInfo::FullName()
{
    return this->szPath;
}

std::string DirectoryInfo::Name()
{
    return DirectoryInfo::GetName(this->szPath);
}

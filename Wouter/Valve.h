// 
// File:   Valve.h
// Author: wouter
//
// Created on 22 november 2007, 13:11
//

#ifndef VALVE_H_
#define VALVE_H_

#include <string>

namespace Valve
{
	namespace Steam
	{
	
	}
	
	namespace GoldSource
	{
	/*
	 * PAK Structures
	 */
		typedef struct sPAKHeader
		{
			char id[4];
			int dir_offset;
			int dir_size;
			
		} tPAKHeader;
		
		typedef struct sPAKLump
		{
			char filename[56];
			int offset;
			int size;
			
		} tPAKLump;

	/*
	 * ENT Structures
	 */
		typedef struct sEntityPair
		{
			sEntityPair *next;
			std::string key;
			std::string value;
			
		} tEntityPair;

		typedef struct sEntity
		{
			float origin[3];
			int firstbrush;
			int numbrushes;
			tEntityPair *epairs;
			
		} tEntity;
		
	/*
	 * WAD Structures
	 */
		typedef struct sWADHeader
		{
			char id[4];
			int numlumps;
			int tableoffset;
			
		} tWADHeader;
		
		typedef struct sWADLump
		{
			int offset;
			int disksize;
			int size;
			
			char type;
			char compression;
			
			char pad1;
			char pad2;
			
			char name[16];
			
		} tWADLump;
		
		const int MIPLEVELS = 4;
		
		typedef struct sMiptex
		{
			char name[16];
			
			unsigned width;
			unsigned height;
			
			unsigned offset[MIPLEVELS];
			
		} tMiptex;
	/*
	 * BSP Structures
	 */
		const int BSPVERSION = 30;
		
		const int BSPLUMP_ENTITIES = 0;
		const int BSPLUMP_PLANES = 1;
		const int BSPLUMP_TEXTURES = 2;
		const int BSPLUMP_VERTEXES = 3;
		const int BSPLUMP_VISIBILITY = 4;
		const int BSPLUMP_NODES = 5;
		const int BSPLUMP_TEXINFO = 6;
		const int BSPLUMP_FACES = 7;
		const int BSPLUMP_LIGHTING = 8;
		const int BSPLUMP_CLIPNODES = 9;
		const int BSPLUMP_LEAFS = 10;
		const int BSPLUMP_MARKSURFACES = 11;
		const int BSPLUMP_EDGES = 12;
		const int BSPLUMP_SURFEDGES = 13;
		const int BSPLUMP_MODELS = 14;
		
		const int MAX_BSP_LUMPS = 15;
		
		typedef struct sBSPLump
		{
			int offset;
			int size;
			
		} tBSPLump;
		
		typedef struct sBSPHeader
		{
			int version;
			tBSPLump lumps[MAX_BSP_LUMPS];
			
		} tBSPHeader;
		
		typedef struct sBSPPlane
		{
			float normal[3];
			float dist;
			int type;
			
		} tBSPPlane;
		
		typedef struct sBSPTexture
		{
			float vecs[2][4];
			int miptex;
			int flags;
			
		} tBSPTexture;
		
		typedef struct sBSPVertex
		{
			float point[3];
			
		} tBSPVertex;
		
		typedef struct sBSPNode
		{
			int planenum;
			short children[2];
			
			short min[3];
			short max[3];
			
			unsigned short firstface;
			unsigned short numfaces;
			
		} tBSPNode;
		
		const int MAXLIGHTMAPS = 4;
		
		typedef struct sBSPFace
		{
			short planenum;
			short side;
			
			int firstedge;
			short numedges;
			
			short texinfo;
			
			unsigned char styles[MAXLIGHTMAPS];
			int lightoffset;
			
		} tBSPFace;
		
		typedef struct sBSPClipNode
		{
			int planenum;
			short children[2];
			
		} tBSPClipNode;
		
		const int NUM_AMBIENTS = 4;
		
		typedef struct sBSPLeaf
		{
			int contents;
			int visibilityoffset;
			
			short min[3];
			short max[3];

			unsigned short firstmarksurface;
			unsigned short nummarksurfaces;

			unsigned char ambient_level[NUM_AMBIENTS];
			
		} tBSPLeaf;
		
		typedef struct sBSPEdge
		{
			unsigned short verts[2];
			
		} tBSPEdge;
		
		const int MAX_MAP_HULLS = 4;
		
		typedef struct sBSPModel
		{
			float min[3];
			float max[3];
			float origin[3];
			
			int headnode[MAX_MAP_HULLS];
			int visleaf;
			
			int firstface;
			int numfaces;
			
		} tBSPModel;
		
	/*
	 * MDL Structures
	 */		
		typedef struct sMDLHeader
		{
			int id;
			int version;
			char name[64];
			int length;
			
		} tMDLHeader;
		
		typedef struct sMDLTable
		{
			float eye[3];
			float min[3];
			float max[3];
			float bbmin[3];
			float bbmax[3];
			int flags;
			
			int numbones;
			int bonesoffset;
			
			int numbonecontrollers;
			int bonecontrolleroffset;
			
			int numhitboxes;
			int hitboxoffset;
			
			int numsequences;
			int sequenceoffset;
			
			int numtextures;
			int textureoffset;
			int texturedataoffset;
			
			int numskinref;
			int numskinfamilies;
			int skinoffset;
			
			int numbodyparts;
			int bodypartoffset;
			
			int numattachments;
			int attachmentoffset;
			
			int numsoundtables;
			int soundoffset;
			
			int numsoundgroups;
			int soundgroupoffset;
			
			int numtransitions;
			int transitionsoffset;
			
		} tMDLTable;
	}
	
	namespace Source
	{
	
	}
}
#endif /*VALVE_H_*/

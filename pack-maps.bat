@echo off
@cls

set "zip=%ProgramFiles%\7-Zip\7z.exe"
set "MOD_FOLDER=%~dp0"

for %%I in (%cd%\maps\*.res) do (
    call "%zip%" -w%MOD_FOLDER% a %%~nI.zip -spf @%%I
    call "%zip%" -w%MOD_FOLDER% a %%~nI.zip -spf maps\%%~nI.bsp
    call "%zip%" -w%MOD_FOLDER% a %%~nI.zip -spf maps\%%~nI.res
    call "%zip%" -w%MOD_FOLDER% a %%~nI.zip -spf maps\%%~nI.txt
)
